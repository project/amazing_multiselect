# INTRODUCTION
The Amazing Multiselect module allows you to use the Multiselect JQuery Library
with search fields.

# REQUIREMENTS
This module require the core Field module.

# INSTALLATION

```ssh
composer require drupal/amazing_multiselect 
```
# CONFIGURATION

You need to create a reference field type with unlimited values in your entity
 and click on "Manage form display" to choose Amazing Multiselect widget. 
